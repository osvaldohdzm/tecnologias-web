<%-- 
    Document   : browser
    Created on : 12/10/2016, 07:37:29 PM
    Author     : Macario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Browser de base de datos</title>
        <link rel="stylesheet" type="text/css" href="estilos\estilos.css" />
    </head>
    <body>
     <img src="imagenes/delfin.jpg" width=220 height=175 />
   
     
     
   <% 
        String mySQLHost = application.getInitParameter("mySQLHost");
	String dbName = application.getInitParameter("dbName");
        String user = application.getInitParameter("user");
        String password = application.getInitParameter("password");

        if(request.getMethod().equals("GET"))
        {    
        try{

			String urlBD = mySQLHost + dbName+ "?user=" + user + "&password=" + password;
			Connection con = DriverManager.getConnection(urlBD);
			Statement  st  = con.createStatement();

			st.executeQuery("show databases");

			ResultSet resultadosQuery = st.getResultSet();%>

<h3>Bases de Datos MySQL Server</h3>
<form name="forma01" method="post" action="browser.jsp">
<table>
<tr><th>Data Base Name</th><tr>
<%
			while(resultadosQuery.next())
			{
				String dataBaseName = resultadosQuery.getString("database");

				if(!dataBaseName.equals("information_schema") && !dataBaseName.equals("mysql") && !dataBaseName.equals("test"))
				{

%>
                                <tr><td><input class="bd" type="submit" name="baseDeDatos"  value=<%=dataBaseName%>></td><tr>
                             <%  }
                        } %>
                        <tr><td>Seleccione una Base de Datos</td></tr></table></form>
<%                }
		catch(Exception ex)
		{
			out.println("<h3>Falló la conexión</h3>");
			out.println(ex.toString());
			ex.printStackTrace(response.getWriter());
		}
        } // End If method = GET
        else if(request.getMethod().equals("POST"))
        {
%>

<h3>Tablas de la base de datos: <%=request.getParameter("baseDeDatos")%></h3>

<%
        }
%>   
    </body>
</html>
