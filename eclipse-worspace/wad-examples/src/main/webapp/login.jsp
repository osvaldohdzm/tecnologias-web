<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core" version="2.0">
	<jsp:directive.page language="java"
		contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" />
	<jsp:text>
		<![CDATA[ <?xml version="1.0" encoding="UTF-8" ?> ]]>
	</jsp:text>
	<jsp:text>
		<![CDATA[ <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> ]]>
	</jsp:text>
	<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Login Example</title>
</head>
<body>
	<c:set var="errores" value="${requestScope['errores']}"/>
	<h1>Ingreso al sistema</h1>
	-<c:out value="${errores}"/>-
	<form action="Ejemplo11A" method="post">
		Usuario: <input type="text" name="user" /><br /> Contraseña: <input
			type="password" name="pass" /> <input type="submit" />
	</form>
</body>
	</html>
</jsp:root>