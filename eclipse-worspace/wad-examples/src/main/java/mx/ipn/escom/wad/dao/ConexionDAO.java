package mx.ipn.escom.wad.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConexionDAO {
	
	protected static Connection getConexion() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/wadLogIn";
			String user = "dany";
			String pass = "b3rum3n";
			return DriverManager.getConnection(url,user,pass);
		}catch(SQLException ex) {
			ex.printStackTrace();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	protected static boolean releaseConnection(Connection con, PreparedStatement ps, ResultSet rs) {
		boolean succes;
		try{
			rs.close();
			ps.close();
			con.close();
			succes =true;
		}catch (SQLException ex){
			succes = false;
		}
		return succes;
	}

}
