package mx.ipn.escom.wad.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException; 
import java.io.PrintWriter;
import java.util.Enumeration;

public class Ejemplo4 extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		out.println("<html>");
		out.println("<head></head>");
		out.println("<body>");
		out.println("<h1>Ejemplo 4: Extracción de parámetros dinámicamente de la QueryString</h1>");		
		out.println("<label>La QueryString es: " + req.getQueryString());
		Enumeration<String> parametros = req.getParameterNames();
		while(parametros.hasMoreElements()){
			String parametro = parametros.nextElement();
			String[] valor = req.getParameterValues(parametro);
			for(int i=0 ; i < valor.length ; i++){
				out.println(parametro + "["+i+"] = " + valor[i] + "<br/>");
			}
		}
		out.println("</body>");
		out.println("</html>");
	}		

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doPut(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doDelete(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doHead(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doTrace(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}

	public void doOptions(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

}
