package mx.ipn.escom.wad.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FieldErrors {
	private Map<String, List<String>> errors;

	private FieldErrors() {
		this.errors = new HashMap<String, List<String>>();
	}

	public static FieldErrors getInstance() {
		return new FieldErrors();
	}

	public void addFieldError(String name, String message) {
		if (this.errors.containsKey(name)) {
			List<String> messages = this.errors.get(name);
			if (!messages.contains(message)) {
				messages.add(message);
				this.errors.put(name, messages);
			}
		} else {
			List<String> messages = new ArrayList<String>();
			messages.add(message);
			this.errors.put(name, messages);
		}
	}

	public Boolean hasFieldErrors() {
		return !this.errors.isEmpty();
	}

	public Map<String, List<String>> getFieldErrors() {
		return errors;
	}

	public List<String> getFieldErrors(String name) {
		List<String> messages = this.errors.get(name);
		if (messages == null) {
			messages = new ArrayList<String>();
		}
		return messages;
	}
}
