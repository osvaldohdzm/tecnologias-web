package mx.ipn.escom.wad.servlet;
/*
 * hiberante core, entitry manager, common annotations, javax.persistence, , org.antlr, org.jboss, cglib, c3po(pool de conmocecciones), jta(javaTransaction manaer)
 * dependencia para el connector....
 * 
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import mx.ipn.escom.wad.bs.LoginBs;
import mx.ipn.escom.wad.entity.Usuario;
import mx.ipn.escom.wad.exception.LoginException;
import mx.ipn.escom.wad.exception.UserLockedException;
import mx.ipn.escom.wad.exception.UserNotFoundException;
import mx.ipn.escom.wad.util.FieldErrors;

/**
 * Servlet implementation class Ejemplo11
 */
public class Ejemplo11A extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private LoginBs loginBs;
	
	private Properties properties;
	
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		properties = new Properties();
		InputStream input;
		try {
			SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
			input = Thread.currentThread().getContextClassLoader().getResourceAsStream("messages.properties");
			properties.load(input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Ejemplo11A() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FieldErrors fieldErrors = FieldErrors.getInstance();
		Usuario usuario = validateForm(fieldErrors, request);
		if (!fieldErrors.hasFieldErrors()) {
			try {
				usuario = loginBs.login(usuario.getLogin(), usuario.getPassword());
				HttpSession session = request.getSession();
				session.setAttribute("usuario", usuario);
				RequestDispatcher rd = request.getRequestDispatcher("welcome.jsp");
				rd.forward(request, response);
			} catch (UserNotFoundException e) {
				RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
				String mensaje = properties.getProperty("MSG1");
				request.setAttribute("errores", mensaje);
				rd.forward(request, response);
			} catch (LoginException e) {
				String mensaje = properties.getProperty("MSG1");
				RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
				request.setAttribute("errores", mensaje);
				rd.forward(request, response);
			} catch (UserLockedException e) {
				String mensaje = properties.getProperty("MSG2");
				RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
				request.setAttribute("errores", mensaje);
				rd.forward(request, response);
			}
		} else {
			String mensaje = "Campos faltantes";
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			request.setAttribute("errores", mensaje);
			rd.forward(request, response);
			for(String name:fieldErrors.getFieldErrors().keySet()) {
				for(String m:fieldErrors.getFieldErrors(name)) {
					System.out.println("Mensaje: "+name+" - "+m);
				}
			}
		}

	}

	private Usuario validateForm(FieldErrors fieldErrors, HttpServletRequest request) {
		Usuario usuario = new Usuario();
		String login = request.getParameter("user");
		
		if (login == null || (login != null && login.equals(""))) {
			fieldErrors.addFieldError("login", properties.getProperty("MSG3"));
		}
		
		String password = request.getParameter("pass");
		
		if (password == null || (password != null && password.equals(""))) {
			fieldErrors.addFieldError("password", properties.getProperty("MSG4"));
		} 
		
		if(!fieldErrors.hasFieldErrors()) {
			usuario.setLogin(login);
			usuario.setPassword(password);
		}
		return usuario;
	}

}
