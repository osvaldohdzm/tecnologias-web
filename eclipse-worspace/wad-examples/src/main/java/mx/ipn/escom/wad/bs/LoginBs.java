package mx.ipn.escom.wad.bs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import mx.ipn.escom.wad.dao.LoginDao;
import mx.ipn.escom.wad.entity.Usuario;
import mx.ipn.escom.wad.exception.LoginException;
import mx.ipn.escom.wad.exception.UserLockedException;
import mx.ipn.escom.wad.exception.UserNotFoundException;

@Service("loginBs")
@Scope(value = BeanDefinition.SCOPE_SINGLETON)

public class LoginBs {
	@Autowired
	private LoginDao loginDao;
	
	public Usuario login(String login, String password)
			throws UserNotFoundException, LoginException, UserLockedException {
		if (login != null && password != null) {
			Usuario usuario = loginDao.login(login, password);
			if (usuario != null) {
				if (!usuario.getBloqueado()) {
					return usuario;
				} else {
					throw new UserLockedException();
				}
			} else {
				throw new UserNotFoundException();
			}
		} else {
			throw new LoginException();
		}
	}
}