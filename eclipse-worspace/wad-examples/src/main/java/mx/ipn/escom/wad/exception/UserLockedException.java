package mx.ipn.escom.wad.exception;

public class UserLockedException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserLockedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserLockedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public UserLockedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserLockedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserLockedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
