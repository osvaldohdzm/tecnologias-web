package mx.ipn.escom.wad.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException; 
import java.io.PrintWriter;

public class Ejemplo3 extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		String x = req.getParameter("x");
		String y = req.getParameter("y");
		String z = req.getParameter("z");
		out.println("<html>");
		out.println("<head></head>");
		out.println("<body>");
		out.println("<h1>Ejemplo 3: Extracción de parámetros de la QueryString</h1>");		
		out.println("La QueryString "+ x +"<br/>");		
		out.println("<label>x: </label> "+ x +"<br/>");
		out.println("<label>y: </label> "+ y +"<br/>");
		out.println("<label>z: </label> "+ z +"<br/>");
		out.println("</body>");
		out.println("</html>");
	}		

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doPut(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doDelete(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doHead(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doTrace(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}

	public void doOptions(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

}
