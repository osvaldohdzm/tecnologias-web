package mx.ipn.escom.wad.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import java.io.IOException; 
import java.io.PrintWriter;

public class Ejemplo5 extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer tiempoBloqueo;
	private Integer numeroIntentos;	


	public void init(ServletConfig config) throws ServletException {
		System.out.println("Inicializando servlet...");
		tiempoBloqueo = Integer.parseInt(config.getInitParameter("tiempoBloqueo"));
		numeroIntentos = Integer.parseInt(config.getInitParameter("numeroIntentos"));
	}	

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		out.println("<html>");
		out.println("<head></head>");
		out.println("<body>");
		out.println("<h1>Ejemplo 5: Parámetros de inicialización</h1>");		
		out.println("<label>Los parámetros de inicialización son:</label>");
		out.println("<ol>");
		out.println("<li> Tiempo de bloqueo: "+tiempoBloqueo+"</li>");
		out.println("<li> Número de intentos: "+numeroIntentos+"</li>");
		out.println("</ol>");
		out.println("</body>");
		out.println("</html>");
	}		

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doPut(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doDelete(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doHead(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doTrace(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}

	public void doOptions(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

}
