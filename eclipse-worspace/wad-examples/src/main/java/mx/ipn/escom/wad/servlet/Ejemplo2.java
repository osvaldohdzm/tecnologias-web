package mx.ipn.escom.wad.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException; 
import java.io.PrintWriter;

public class Ejemplo2 extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void init() throws ServletException {
		System.out.println("Inicializando servlet...");
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		res.setContentType("text/html");
		String mensaje="Atendiendo petición...";
		PrintWriter out = res.getWriter();
		out.println("<html>");
		out.println("<head></head>");
		out.println("<body>");
		out.println("<h1>Ejemplo2: Ciclo de vida de un Servlet</h1>");
		out.println("mensaje: "+mensaje);		
		out.println("</body>");
		out.println("</html>");
		System.out.println(mensaje);
	}		

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doPut(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doDelete(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doHead(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		

	public void doTrace(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}

	public void doOptions(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{

	}		
	
	public void destroy(){
		System.out.println("Destruyendo servlet...");
	}
}
