package mx.ipn.escom.wad.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Ejemplo6
 */
public class Ejemplo6 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Ejemplo6() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<head></head>");
		out.println("<body>");
		out.println("<h1>Ejemplo 6: Sessiones HTTP (parte 1)</h1>");
		HttpSession session = request.getSession();
		out.print("<ul>");
		out.println("<li>Id: " + session.getId() + "</li>");
		Date date = new Date();
		date.setTime(session.getCreationTime());
		out.println("<li>Creation Time: " + date + "</li>");
		out.println("<li>Last Accessed Time: " + session.getLastAccessedTime() + "</li>");
		out.println("<li>Max Inactive Interval: " + session.getMaxInactiveInterval() + "</li>");
		Enumeration<String> attributtesNames = session.getAttributeNames();
		out.println("<li>Attribute Names:");
		out.println("<ul>");
		while(attributtesNames.hasMoreElements()) {
			out.println("<li>Attribute: "+attributtesNames.nextElement()+"</li>");
		}
		out.println("</ul>");
		out.println("</li>");
		out.print("</ul>");
		out.println("</body>");
		out.println("</html>");
		session.setAttribute("fecha", date);
		session.setAttribute("usuario", "hermes.montes@gmail.com");
		List<String> list = new ArrayList<String>();
		list.add("Andrés Manuel López Obrador");
		list.add("Ricardo Anaya Córtez");
		list.add("José Antonio Meade");
		list.add("Margarita Zavala");
		session.setAttribute("candidatos", list);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
