package mx.ipn.escom.wad.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import mx.ipn.escom.wad.entity.Usuario;

@Service("loginDao")
@Scope(value = BeanDefinition.SCOPE_SINGLETON)

public class LoginDao extends ConexionDAO{
	
	public Usuario login(String user, String pass){
        Usuario us=null;
        Connection conn=null;
        PreparedStatement ps=null;
        ResultSet rs=null;
        if(user == null || pass ==null) {
        	us = null;
        }
        try{
            conn=getConexion();
            
            String sql="SELECT * FROM Usuarios WHERE login=? AND password=?;";
            ps =conn.prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, pass);
            rs=ps.executeQuery();
            if(rs.next()){
                us=new Usuario();
                us.setNombre(rs.getString("nombre"));
                us.setPrimerApellido(rs.getString("primerApellido"));
                us.setSegundoApellido(rs.getString("segundoApellido"));
                us.setLogin(rs.getString("login"));
                us.setBloqueado(rs.getBoolean("bloqueado"));
            }else {
            	us = null;
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            us=null;
        }finally {
        	if(!releaseConnection(conn, ps, rs)){
        		releaseConnection(conn, ps, rs);
        	}
        }
        return us;
    }
		
}
