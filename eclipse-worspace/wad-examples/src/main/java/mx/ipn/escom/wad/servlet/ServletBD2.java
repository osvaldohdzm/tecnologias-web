/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.ipn.escom.wad.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

/**
 *
 * @author Macario
 */
public class ServletBD2 extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
        private String mySQLHost;
        private String user;
        private String password;

    @Override
    public void init(ServletConfig config) throws ServletException
    {
    	super.init(config);
        mySQLHost = getServletContext().getInitParameter("mySQLHost");
        user = getServletContext().getInitParameter("user");
        password = getServletContext().getInitParameter("password");
    }

    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter ();
    	response.setContentType("text/html;charset=UTF-8");

    	encabezado(out);
    	
        	String bd = request.getParameter("baseDeDatos");
        	String tb = request.getParameter("tableName");
        	String rows = "";

        	if(bd!=null) out.println("<h3>Mostrar tabla: " + bd + "</h3>");
        	
        	
        	// Muestra las bases de datos MySQL

        	try
        	{

        		String urlBD = "jdbc:mysql://" + mySQLHost + "/" + bd + "?useSSL=false&useUnicode=yes&characterEncoding=UTF-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC" + "&user=" + user + "&password=" + password;
        		
        		
        		Connection con = DriverManager.getConnection(urlBD);
        		Statement  st  = con.createStatement();

        		st.executeQuery("select * from "+bd + "." + tb);

        		ResultSet resultadosQuery = st.getResultSet();
           		 int rowCount = 0;
        		 ResultSetMetaData rsmd = resultadosQuery.getMetaData();

        		out.println("<h3>Bases de Datos MySQL Server</h3>");
        		// El action del formulario envía al mismo servlet usando POST
                        out.println("<form name=\"forma02\" method=\"post\" action=\"ServletBD\">");
                        out.println("<table>");


                        int columnCount = rsmd.getColumnCount();
                        // table header
                        out.println("<TR>");
                        for (int i = 0; i < columnCount; i++) {
                          out.println("<TH>" + rsmd.getColumnLabel(i + 1) + "</TH>");
                          }
                        out.println("</TR>");
                        // the data
                        while (resultadosQuery.next()) {
                         rowCount++;
                         out.println("<TR>");
                         for (int i = 0; i < columnCount; i++) {
                           out.println("<TD>" + resultadosQuery.getString(i + 1) + "</TD>");
                           }
                         out.println("</TR>");
                         }
        	
        	
                        out.println("<input type=\"hidden\" value=\"" + rows  + "\" name=\"tableName\" /></table></form>");
                    
    }
        	catch (Exception ex)
        	{
                    out.println("<h3>Falló la conexión</h3>");
                    out.println(ex.toString());
                    ex.printStackTrace(out);
        	}

	    out.println("</body>");
	    out.println("</html>");
		out.flush();
		out.close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected void encabezado(PrintWriter out)
    {
		out.println("<!DOCTYPE HTML>");
   		out.println("<html><head>");
                out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"estilos\\estilos.css\">");
		out.println("</head><body>");
		out.println("<img src=\"resources/imagenes/delfin.jpg\" width=220 height=175 />");
    }
 }
