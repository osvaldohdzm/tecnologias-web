<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" >
		<xsl:template match="/">
			<html lang="en">
			<head>
				<title>Top 3</title>
				 <link href="css/estilo-tabla.css" rel="stylesheet" type="text/css" /> 
			</head>
			<body>
				<table >
					<thead>
					<tr>
						<th>Title</th>
						<th>Directed by</th>
						<th>Writter</th>
						<th>Genre</th>
						<th>Video</th>
					</tr>
					</thead>
					<tbody >
					<xsl:for-each select="cine/movie">
						<tr>
							    <td>
							    	<xsl:value-of select="title"/> <br/>
							    </td>
								<td><xsl:value-of select="director"/></td>
							    <td>
										  <xsl:value-of select="writter"/><br/>
							    </td>
							    <td><xsl:value-of select="genre"/></td>
							    <td class="imagen">
								<xsl:variable name="archivo-video"> <xsl:value-of select="video"/></xsl:variable>
									  <object data="http://www.youtube.com/v/{$archivo-video}"
												  type="application/x-shockwave-flash" width="212" height="170" >
									 </object>								
							</td>
						</tr>
					</xsl:for-each>
					</tbody>
					</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
