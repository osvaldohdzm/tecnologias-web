<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" >
		<xsl:template match="/">
			<html lang="en">
			<head>
				<title>Top 3</title>
				 <link href="css/estilo-tabla.css" rel="stylesheet" type="text/css" /> 
			</head>
			<body>
				<table >
					<thead>
					<tr>
						<th>Tecnology</th>
						<th>Invented by</th>
						<th>Video information</th>
					</tr>
					</thead>
					<tbody >
					<xsl:for-each select="technologies/gadget">
						<tr>
							    <td>
							    	<xsl:value-of select="technology"/> <br/>
							    </td>
								<td><xsl:value-of select="inventor"/></td>
							    <td class="imagen">
								<xsl:variable name="archivo-video"> <xsl:value-of select="video"/></xsl:variable>
									  <object data="http://www.youtube.com/v/{$archivo-video}"
												  type="application/x-shockwave-flash" width="212" height="170" >
									 </object>								
							</td>
						</tr>
					</xsl:for-each>
					</tbody>
					</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
